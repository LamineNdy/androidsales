package android.graph;

import android.content.Intent;
import android.view.View;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ProgressBar;
import org.achartengine.model.CategorySeries;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;


public class GraphAChartEngineActivity extends Activity {
    /** Called when the activity is first created. */
    private String amount;
    private String total;
    private EditText soldView;
    private EditText totalView;
    private ProgressBar pg;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        soldView=(EditText)findViewById(R.id.sold);
        totalView = (EditText)findViewById(R.id.total);
        pg=(ProgressBar)findViewById(R.id.ring);
        new SoldRequestTask().execute("http://10.0.2.2:8080/SupSalesAnalytics/webresources/sales/sold");
        new countRequestTask().execute("http://10.0.2.2:8080/SupSalesAnalytics/webresources/sales/count");

    }


    public void pieGraphHandler (View view)
    {
        this.pg.setVisibility(View.VISIBLE);
        new breakDownRequestTask().execute("http://10.0.2.2:8080/SupSalesAnalytics/webresources/sales/AgencyBreakdown");
    }

    public void channelBreakdownHandler(View view){
        this.pg.setVisibility(View.VISIBLE);
        new breakDownRequestTask().execute("http://10.0.2.2:8080/SupSalesAnalytics/webresources/sales/channelBreakdown");
    }

    public void maritalBreakdownHandler(View view){
        this.pg.setVisibility(View.VISIBLE);
        new breakDownRequestTask().execute("http://10.0.2.2:8080/SupSalesAnalytics/webresources/sales/maritalBreakdown");
    }

    public void genderBreakdownHandler(View view){
        this.pg.setVisibility(View.VISIBLE);
        new breakDownRequestTask().execute("http://10.0.2.2:8080/SupSalesAnalytics/webresources/sales/genderBreakdown");
    }
    public void incomeBreakdownHandler(View view){
        this.pg.setVisibility(View.VISIBLE);
        new breakDownRequestTask().execute("http://10.0.2.2:8080/SupSalesAnalytics/webresources/sales/incomeBreakdown");
    }

    protected class SoldRequestTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... uri) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;
            String responseString = null;
            try {
                response = httpclient.execute(new HttpGet(uri[0]));
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    response.getEntity().writeTo(out);
                    out.close();
                    responseString = out.toString();
                } else{
                    //Closes the connection.
                    response.getEntity().getContent().close();
                    throw new IOException(statusLine.getReasonPhrase());
                }
            } catch (ClientProtocolException e) {
                //TODO Handle problems..
            } catch (IOException e) {
                //TODO Handle problems..
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("RESPONSE", "The response is: " + result);
            amount=result;
            soldView.setText(amount);
        }

    }

    protected class countRequestTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... uri) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;
            String responseString = null;
            try {
                response = httpclient.execute(new HttpGet(uri[0]));
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    response.getEntity().writeTo(out);
                    out.close();
                    responseString = out.toString();
                } else{
                    //Closes the connection.
                    response.getEntity().getContent().close();
                    throw new IOException(statusLine.getReasonPhrase());
                }
            } catch (ClientProtocolException e) {
                //TODO Handle problems..
            } catch (IOException e) {
                //TODO Handle problems..
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("RESPONSE", "The response is: " + result);

            total=result;
            totalView.setText(total);
            return;

        }

    }


    protected class breakDownRequestTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... uri) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;
            String responseString = null;
            try {
                response = httpclient.execute(new HttpGet(uri[0]));
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    response.getEntity().writeTo(out);
                    out.close();
                    responseString = out.toString();
                } else{
                    //Closes the connection.
                    response.getEntity().getContent().close();
                    throw new IOException(statusLine.getReasonPhrase());
                }
            } catch (ClientProtocolException e) {
                //TODO Handle problems..
            } catch (IOException e) {
                //TODO Handle problems..
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("BREAKDOWN", "The response is: " + result);

            JSONArray array = null;
            try {
                array = new JSONArray(result);
            } catch (JSONException e) {
                Log.d("PROBLEM",e.getMessage());
            }
            CategorySeries series = new CategorySeries("BreakDown");
            CategorySeries serie2 = new CategorySeries("Breakdown");
            for (int i = 0; i < array.length(); i++) {

                 JSONObject row = null;
                try {
                    row = array.getJSONObject(i);
                } catch (JSONException e) {
                    Log.d("PROBLEM",e.getMessage());
                }
                for (Iterator iterator = row.keys(); iterator.hasNext();) {
                        Object cle = iterator.next();
                        Object val = null;
                    try {
                        val = row.get(String.valueOf(cle));
                    } catch (JSONException e) {
                        Log.d("PROBLEM",e.getMessage());
                    }
                    if(array.length()>3){
                        if(i%2==0)
                            series.add((String) cle,(Integer)val);
                        else
                            serie2.add((String) cle,(Integer)val);
                    }
                    else {
                        series.add((String) cle,(Integer)val);
                   }
                }
            }

            if(array.length()>3)
            {
                BarGraph pie = new BarGraph();
                Intent lineIntent = pie.getIntent(GraphAChartEngineActivity.this,series,serie2);
                pg.setVisibility(View.INVISIBLE);
                startActivity(lineIntent);
            }
            else{
                PieGraph pie = new PieGraph();
                Intent lineIntent = pie.getIntent(GraphAChartEngineActivity.this,series, array.length());
                pg.setVisibility(View.INVISIBLE);
                startActivity(lineIntent);
            }

        }

    }

}